package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),View.OnClickListener{
    private var firstVariable:Double=0.0
    private var secondVariable:Double=0.0
    private var operation=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init(){
        oneButton.setOnClickListener(this)
        twoButton.setOnClickListener(this)
        threeButton.setOnClickListener(this)
        fourButton.setOnClickListener(this)
        fiveButton.setOnClickListener(this)
        sixButton.setOnClickListener(this)
        sevenButton.setOnClickListener(this)
        eightButton.setOnClickListener(this)
        nineButton.setOnClickListener(this)
        zeroButton.setOnClickListener(this)
        dotButton.setOnClickListener {
            if(resultTextView.text.isNotEmpty() && "." !in resultTextView.text.toString()){
                resultTextView.text = resultTextView.text.toString() + "."
            }
        }

    }
    fun backspace(view: View){
        val value=resultTextView.text.toString()
        if (value.isNotEmpty()){
            resultTextView.text=value.substring(0,value.length-1)
        }
    }
    fun equal(view: View){
        val value=resultTextView.text.toString()
        if (value.isNotEmpty()) {
            secondVariable = value.toDouble()
            var result:Double = 0.0
            if (operation == ":") {
                result = firstVariable / secondVariable
            }
            if (operation=="x"){
                result = firstVariable * secondVariable
            }
            if (operation=="+"){
                result = firstVariable + secondVariable
            }
            if(operation=="-"){
                result=firstVariable-secondVariable
            }
            resultTextView.text = result.toString()
            firstVariable=0.0
            secondVariable=0.0
            operation=""
        }
    }
    fun division(view: View){
        val value=resultTextView.text.toString()
        if (value.isNotEmpty()){
            firstVariable=value.toDouble()
            operation=":"
            resultTextView.text=""
        }
    }
    fun substraction(view: View){
        val value=resultTextView.text.toString()
        if(value.isNotEmpty()){
            firstVariable=value.toDouble()
            operation="-"
            resultTextView.text=""
        }
    }
    fun multifly(view: View){
        val value=resultTextView.text.toString()
        if (value.isNotEmpty()){
            firstVariable=value.toDouble()
            operation="x"
            resultTextView.text=""
        }
    }

    fun addition(view: View){
        val value=resultTextView.text.toString()
        if (value.isNotEmpty()){
            firstVariable=value.toDouble()
            operation="+"
            resultTextView.text=""
        }
    }

    override fun onClick(v: View?) {
        val button = v as Button
        resultTextView.text= resultTextView.text.toString()+button.text.toString()
    }
}